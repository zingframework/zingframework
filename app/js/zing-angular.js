/*!
 * Copyright 2014 Codefuse.
 * http://www.codefuse.com.au/
 *
 */

(function() {
var ZingModule = angular.module('zing', ['ngAnimate', 'ngSanitize', 'ui.router','ui.bootstrap']),
    extend = angular.extend,
    forEach = angular.forEach,
    isDefined = angular.isDefined,
    isNumber = angular.isNumber,
    isString = angular.isString,
    jqLite = angular.element;

/**
 * @license Zing 1.0.0
 * @author : Sundar(msg.sundar@gmail.com)
 */

ZingModule
    .factory('$$ionicAttachDrag', ['$document', '$window',function($document,$window) {

    }]);
})();