var ZingModule = angular.module('zing', ['ngAnimate', 'ngSanitize', 'ui.router','ui.bootstrap']),
    extend = angular.extend,
    forEach = angular.forEach,
    isDefined = angular.isDefined,
    isNumber = angular.isNumber,
    isString = angular.isString,
    jqLite = angular.element;
