var pkg = require('../package.json');
var fs = require('fs');

/*
 var DISCOURSE_FILE = __dirname + '/DISCOURSE_POST_URL';
 releasePostUrl: fs.readFileSync(DISCOURSE_FILE).toString(),
 releasePostFile: DISCOURSE_FILE,
 */

module.exports = {
    dist: 'app',
    protractorPort: 8876,
    banner: '/*!\n' +
        ' * Copyright 2014 Codefuse.\n' +
        ' * http://www.codefuse.com.au/\n' +
        ' *\n' +
        ' */\n\n',
    bundleBanner: '/*!\n\n\n',

    closureStart: '(function() {\n',
    closureEnd: '\n})();',

    zingFiles: [
        // Base
        //Compiled in the gulp scripts
        'app/js/zing.js',
        'app/js/utils/**/*.js',
        'app/js/views/**/*.js'
    ],


    zingBundleFiles: [
        //Any files in this array will be bundled up as the total zing js
        //After addition of the banner from above.
        'app/js/zing.js'
    ],
    angularZingFiles: [
        'app/js/angular/*.js',
        'app/js/angular/service/**/*.js',
        'app/js/angular/controller/**/*.js',
        'app/js/angular/directive/**/*.js'
    ],
    vendor: {
        src: "config/lib/**/*.js",
        dest: "app/vendor"
    },

    //Which vendor files to include in dist, used by build
    //Matched relative to config/lib/
    tmpvendorFiles: [
        'fonts/ionicons.eot',
        'fonts/ionicons.svg',
        'fonts/ionicons.ttf',
        'fonts/ionicons.woff'
    ],

    tmpZingBundleFiles: [
        'js/angular/angular.js',
        'js/angular/angular-animate.js',
        'js/angular/angular-sanitize.js',
        'js/angular-ui/angular-ui-router.js',
        'js/ionic-angular.js'
    ],
    //Exclamation can be no longer than 14 chars
    exclamations: [
        "Ah","Aha","Yippee!!","Alrighty!","Good news folks."
    ],
    versionAddress:[
        "A new version of zing has been released.",
        "Codefuse has released another exciting version of zing. "
    ],
    releaseMessage: function () {
        return this.exclamations[Math.floor(Math.random() * this.exclamations.length)] + '! ' +
            this.versionAddress[Math.floor(Math.random() * this.versionAddress.length)]
            + pkg.version + ' "' + pkg.codename + '"! ' +
            this.releasePostUrl;
    }

};