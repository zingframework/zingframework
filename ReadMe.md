### Zing ###
The base framework for the housing application built by Codefuse. This is just a working title for now . It has not been officially named.

```
git clone <repository url> 
git clone git@bitbucket.org:zingframework/zingframework.git
```
to get the repository

To compile the framework you need the following preinstalled
* NodeJs / npm
* bower
* gulp
* grunt

Package.json file has been set up to install all required dev dependencies. In case of adding any more dependencies 
```
npm install 
```
will install all required development dependencies



In case of installing any bower modules,
```
bower install 
```
By default, running npm will install all bower modules as well.


Gulp, is used for running any associated tasks required for setting up the modules. 

By default, running the gulp command will compile any sass files in the sass folder and also combine all the javascript files to a bundle.
```
gulp
```

Here is an non-exhaustive list of all gulp commands
```
gulp build  //This is an alias for the default command . Compiles SaaS and Js
gulp validate
gulp external //Will combine all vendor files into 1 file
gulp watch //Will initialise an watch on all sass files and js files
gulp bundle //This will create an js bundle 

```

-------------

Look at the (demos/sample.js)[demos/sample.js]  file to get a basic understanding on how to build the framework module. Very rough but it will get tehre sometime.


###@TODO : Eventually write a better help ###