/**
 * What AM I GOING TO DO??
 */

//Initialize a global namespace
//Initialize a variable
//Define an angular js app

var appAlpha = angular.module('appAlpha', ['ui.router']);




appAlpha.config(function($stateProvider,$urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/settings");

    $stateProvider
        .state('settings',{
            url:'/settings',
            templateUrl:"partials/layout/settings.html",
            controller:function($scope){

            }
        })
        .state('settings.header', {
            url: "/header",
            templateUrl: "partials/settings/settings.header.html"
        })
        .state('settings.content', {
            url: "/header",
            templateUrl: "partials/settings/settings.content.html"
        })
        .state('settings.footer', {
            url: "/header",
            templateUrl: "partials/settings/settings.footer.html"
        });








    // Now set up the states
    $stateProvider
        .state('state1', {
            url: "/state1",
            templateUrl: "partials/state1.html",
            controller:function($scope){
                alert('state1');
            }
        })
        .state('state1.list', {
            url: "/list",
            templateUrl: "partials/state1.list.html",
            controller: function($scope) {
                $scope.items = ["A", "List", "Of", "Items"];
            }
        })
        .state('state2', {
            url: "/state2",
            templateUrl: "partials/state2.html"
        })
        .state('state2.list', {
            url: "/list",
            templateUrl: "partials/state2.list.html",
            controller: function($scope) {
                $scope.things = ["A", "Set", "Of", "Things"];
            }
        })
        .state('index', {
            url: "/index",
            views: {
                "viewA": { template: "index.viewA" },
                "viewB": { template: "index.viewB" }
            },
            controller:function($scope){
                alert('index');
            }
        })
        .state('route1', {
            url: "/route1",
            views: {
                "viewA": { template: "route1.viewA" },
                "viewB": { template: "route1.viewB" }
            }
        })
        .state('route2', {
            url: "/route2",
            views: {
                "viewA": { template: "route2.viewA" },
                "viewB": { template: "route2.viewB" }
            }
        })
});